-- Problem 1
-- Complete
p1 :: Int
p1 = sum ([x | x <- [1..999], x `mod` 3 == 0 || x `mod` 5 == 0])

-- Problem 2
-- Call with n = 32 as the 32nd fib # is the last one < 4000000
-- Works, but dirt slow.
fib :: (Integral a) => a -> a
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

p2 :: Int -> Int
p2 xs = sum ([ fib x | x <- [1..xs], fib x <= 4000000, fib x `mod` 2 == 0])

fibList :: (Integral a) => a -> [a]
fibList 0 = []
fibList 1 = fib 1 : []
fibList n = fib n : fibList (n - 1)

sumEvenFibs :: (Integral a) => a -> a
sumEvenFibs n = sum (filter (even) (fibList n))

-- Problem 3
-- Complete
isPrime :: (Integral a) => a -> Bool
isPrime n = not (or (map (== 0) (map (mod n) [2..(n - 1)])))

p3 :: (Integral a) => a -> a
p3 n
  | isPrime n = n
  | otherwise = p3 (head ([div n x | x <- [2..n], (mod n x == 0), isPrime x]))

